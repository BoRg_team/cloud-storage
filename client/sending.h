#ifndef sending1
#define sending1

#include <stdio.h>
#include "../overall/functions.h"



int Sending(char *name_of_file, char *account) // отправка данных
{
    FILE *in;
    char data_file[4096]; 
    int sended,b,size,i=0;
    bzero(&sign,sizeof(sign));
    name_of_file[strlen(name_of_file)-1]='\0';
    printf("Имя файла: %s\n",name_of_file);
    in = fopen(name_of_file,"rb");
    if (in == NULL)
    {
        printf("Ошибка, файл не существует\n");
        return 0;
    }
    while(!feof(in)) 
    {
        b=fread(data_file,1,sizeof(data_file),in);
        printf("bytes read: %d, part:%d \n",b,i);
        if(b!=0)
        {
            memcpy(sign.name, PROTOCOL_NAME,4);
            memcpy(sign.version, PROTOCOL_VERSION,4);
            sign.type = PROTOCOL_DATA;
            memcpy(sign.login_name,account,40);
            memcpy(sign.email,name_of_file,60);
            sign.size_data = b;
            memcpy(sign.data,&b,4096);
            send(description,&sign,sizeof(sign),0);
        }
        i++;
    }
    fclose(in);
    return 1;
}

void Select_file(char *account)
{
    char mnt1[512];
    char mnt2[512];
    char Path_to_File[50];
    char FileName[40];
    char *index_of_end;
    int i;
    getcwd(mnt1, sizeof(mnt1));
    bzero(mnt2,sizeof(mnt2));
    while(1)
    {
        printf("Вы находитесь в папке: %s\n",mnt1);
        printf("Введите папку или _send, чтобы выбрать файл: ");
        bzero(Path_to_File,sizeof(Path_to_File));
        fgets(Path_to_File,sizeof(Path_to_File),stdin);
        Path_to_File[strlen(Path_to_File)-1]='\0';
        printf("%s",Path_to_File);
        if(strncmp(Path_to_File,"../",sizeof(Path_to_File))==0)
        {
            index_of_end = strrchr(mnt1,'/');
            strncpy(mnt2,mnt1,index_of_end-&mnt1[0]);
            if (chdir(mnt2))
            {
                printf("Такой папки не существует!\n");
            }
            else
            {
                strcpy(mnt1,mnt2);
            }
            bzero(mnt2,sizeof(mnt2));
        }
        else if(strncmp(Path_to_File,"_list",sizeof(Path_to_File)) == 0)
        {  
            List_of_files();
        }
        else if(strncmp(Path_to_File,"_send",sizeof(Path_to_File))==0) break;
        else if ((strchr(Path_to_File,'/') == NULL) && \
                (strchr(Path_to_File,'.') == NULL) && \
                (strlen(Path_to_File) != 0))
        {
            strcpy(mnt2,mnt1);
            strcat(mnt2,"/");
            strcat(mnt2,Path_to_File);
            if (chdir(mnt2))
            {
                printf("Такой папки не существует!\n");
            } 
            else
            {
                strcpy(mnt1,mnt2);
            }
            bzero(mnt2,sizeof(mnt2));
        }
    } 
    while(1)
    {
        printf("Выберите файл из предложенных: \n");
        List_of_files();
        fgets(Path_to_File,sizeof(Path_to_File),stdin);
        if(Sending(Path_to_File,account)==1)
        {
            printf("Передача успешно выполнена");
        }
    }
    printf("Файл выбран");
}

#endif 