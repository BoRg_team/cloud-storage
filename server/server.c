#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>


#include "../overall/functions.h"
#include "../protocol/protocol.h"

struct sockaddr_in server, client;
struct _PROTOCOL sign;


struct Database
{
    char login[40];
    char password[40];
    char email[60];
} Database;

int Creating_Binding() // Создание готовности принятия клиента
{
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 5445 );
    //Если связка не получилась (-1), то выводим ошибку.
    if( bind(description,(struct sockaddr *)&server,sizeof(server)) == -1)
    {
        printf("Error binding\n");
        exit(1);
    }
    // Иначе продолжаем
    printf("Binding is done\n");
    return 0;
}

void Start_To_Listen()
{
    listen(description,10);
    printf("I'll wait for package\n");
}

int Accepted_package()
{
    int size;
    int recieved;
    int accepting;
    int flag = 0;
    char Path_to_Database[50];
    FILE *file;
    FILE *bd;
    char server_reply[4096];
    size = sizeof(struct sockaddr_in);
    accepting = accept(description, (struct sockaddr*)NULL, NULL);
    if (accepting==-1)
    {
        printf("Can't connect to client\n");
        exit(1);
    }
    for(;;)
    {
        //=====================================================================
        bzero(&Database,sizeof(Database));
        bzero(&sign,sizeof(sign));
        //=====================================================================
        recieved = recv(accepting, &sign , sizeof(sign), 0);
        if (memcmp(sign.name, PROTOCOL_NAME, 4)!=0)
        {
            printf("Это не мой протокол!\n");
            continue;
        }
        if (memcmp(sign.version, PROTOCOL_VERSION, 4)!=0)
        {
            printf("Неверная версия протокола!\n");
            continue;
        }
        printf("\n\n\nПоступил новый пакет данных\n");
        switch (sign.type)
        {
            case PROTOCOL_SIGN_IN:
                
                printf("Полученные данные:\n");
                printf("Запрос на вход\n");
                printf("Логин: %s\n", sign.login_name);
                printf("Пароль: %s\n", sign.password_name);

                bd = fopen("database_users.txt","r");
                if (bd == NULL)
                {
                    printf("Файл не найден!\n");
                    exit(3);
                }
                while(!feof(bd))
                {
                    fscanf(bd,"%s %s %s",Database.login,\
                                         Database.password,\
                                         Database.email);
                    if (strcmp(Database.login, sign.login_name) == 0)
                    {
                        if (strcmp(Database.password, sign.password_name) == 0)
                        {
                            flag = 1;
                            memcpy(sign.name, PROTOCOL_NAME,4);
                            memcpy(sign.version, PROTOCOL_VERSION,4);
                            sign.type = PROTOCOL_ANSWER_YES;
                            send(accepting,&sign,sizeof(sign),0);
                            printf("Отправлено!\n");
                            break;
                        }
                    }
                }
                fclose(bd);
                if (flag == 0)
                {
                    memcpy(sign.name, PROTOCOL_NAME,4);
                    memcpy(sign.version, PROTOCOL_VERSION,4);
                    sign.type = PROTOCOL_ANSWER_NO;
                    send(accepting,&sign,sizeof(sign),0);
                    printf("Отправлено!\n"); 
                    break;
                }
                flag = 0;
                break;
            case PROTOCOL_NEW_USER:
                printf("Полученные данные:\n");
                printf("Запрос на регистрацию\n");
                printf("Логин: %s\n", sign.login_name);
                printf("Пароль: %s\n", sign.password_name);
                printf("Почта: %s\n", sign.email);
                bd = fopen("database_users.txt","r");
                if (bd == NULL)
                {
                    printf("Файл не найден!\n");
                    exit(3);
                }
                while(!feof(bd))
                {
                    fscanf(bd,"%s %s %s",Database.login,\
                                         Database.password,\
                                         Database.email);
                    if (strcmp(Database.login, sign.login_name) == 0)
                    {
                        memcpy(sign.name, PROTOCOL_NAME,4);
                        memcpy(sign.version, PROTOCOL_VERSION,4);
                        sign.type = PROTOCOL_ANSWER_NEW_USER_NO;
                        send(accepting,&sign,sizeof(sign),0);
                        printf("Отправлено!\n"); 
                        flag = 1;
                        break;
                    }
                    else if (strcmp(Database.email, sign.email) == 0)
                    {
                        memcpy(sign.name, PROTOCOL_NAME,4);
                        memcpy(sign.version, PROTOCOL_VERSION,4);
                        sign.type = PROTOCOL_ANSWER_NEW_USER_NO;
                        send(accepting,&sign,sizeof(sign),0);
                        printf("Отправлено!\n"); 
                        flag = 1;
                        break;
                    }
                }
                fclose(bd);
                if (flag == 0)
                {
                    bd = fopen("database_users.txt","a");
                    fprintf(bd,"%s %s %s\n", sign.login_name, \
                                             sign.password_name, \
                                             sign.email);
                    fclose(bd);
                    memcpy(sign.name, PROTOCOL_NAME,4);
                    memcpy(sign.version, PROTOCOL_VERSION,4);
                    sign.type = PROTOCOL_ANSWER_NEW_USER_YES;
                    send(accepting,&sign,sizeof(sign),0);   
                    printf("Отправлено!\n");
                    memcpy(Path_to_Database, "Users_storage/",20);
                    strncat(Path_to_Database, \
                    sign.login_name,sizeof(sign.login_name));
                    printf("%s\n",Path_to_Database);
                    if (mkdir(Path_to_Database))
                    {
                        printf("Папка не была создана!\n");
                    }
                    chmod(Path_to_Database,4555);
                }
                flag = 0;
                break;
            case PROTOCOL_TAKE_A_PATH:
                printf("Полученные данные:\n");
                printf("Запрос на путь\n");
                printf("Логин: %s\n", sign.login_name);
                List_of_files();
                memcpy(sign.name, PROTOCOL_NAME,4);
                memcpy(sign.version, PROTOCOL_VERSION,4);
                sign.type = PROTOCOL_TAKE_A_PATH;
                memcpy(Path_to_Database, "Users_storage/",20);
                strncat(Path_to_Database, \
                sign.login_name,sizeof(sign.login_name));
                memcpy(sign.email,Path_to_Database,60);
                send(accepting,&sign,sizeof(sign),0);
                printf("Отправлено!\n");
                break;
            case PROTOCOL_ERASE_ACCOUNT:
                printf("Полученные данные:\n");
                printf("Удаление аккаунта\n");
                printf("Логин: %s\n", sign.login_name);
                memcpy(Path_to_Database, "Users_storage/",20);
                strncat(Path_to_Database, \
                sign.login_name,sizeof(sign.login_name));
                bd = fopen("database_users.txt","r");
                file = fopen("TMPdatabase_users.txt","w");
                if (bd == NULL)
                {
                    printf("Файл не найден!\n");
                    exit(3);
                }
                while(!feof(bd))
                {
                    if(fscanf(bd,"%s %s %s",Database.login,\
                                         Database.password,\
                                         Database.email))
                    {
                        printf("%s %s %s",   Database.login,\
                                             Database.password,\
                                             Database.email);
                        if (strcmp(Database.login, sign.login_name) != 0)
                        {
                            fprintf(file, "%s %s %s\n", Database.login,\
                                             Database.password,\
                                             Database.email);
                        }
                    }
                }
                fclose(bd);
                fclose(file);
                remove("database_users.txt");
                rename("TMPdatabase_users.txt","database_users.txt");
                if ((rmdir(Path_to_Database))!= 0)
                {
                    printf("Ошибка удаления!\n");
                    memcpy(sign.name, PROTOCOL_NAME,4);
                    memcpy(sign.version, PROTOCOL_VERSION,4);
                    sign.type = PROTOCOL_ERASE_ACCOUNT_NO;
                    send(accepting,&sign,sizeof(sign),0);
                    printf("Отправлено!\n");
                } 
                else
                {
                    printf("Аккаунт успешно удален!\n");
                    memcpy(sign.name, PROTOCOL_NAME,4);
                    memcpy(sign.version, PROTOCOL_VERSION,4);
                    sign.type = PROTOCOL_ERASE_ACCOUNT_YES;
                    send(accepting,&sign,sizeof(sign),0);
                    printf("Отправлено!\n");
                }
                break;
            case PROTOCOL_DATA:
                printf("Полученные данные:\n");
                printf("Файл от %s\n",sign.login_name);
                printf("Размер файла: %d\n",sign.size_data);
                memcpy(Path_to_Database, "Users_storage/",20);
                strncat(Path_to_Database, \
                sign.login_name,sizeof(sign.login_name));
                printf("%s\n",Path_to_Database);
                chdir(Path_to_Database);
                file = fopen(sign.email,"a");
                if (file == NULL)
                {
                    printf("Ошибка в создании файла!");
                    //Дописать отправку ошибки на клиент
                    break;
                }
                fwrite(sign.data,1,sign.size_data,file);
                fclose(file);
                chdir(".../");
                break;
            default:
                printf("Неизвестный пакет\n");   
        }
    }
}


int main()
{
    Creating_Socket(); // Создание сокета сервера из functions.h
    Creating_Binding(); 
    Start_To_Listen();
    Accepted_package();
    return 0;
}