K      �;	#ifndef client
#define client
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

#include "../overall/functions.h"
#include "../Registration/Registration.h"
#include "sending.h"
#include "Commands.h"

struct sockaddr_in server;
struct _PROTOCOL answering;

char account[40];

/*
void Sending() // отправка данных
{
    char message[1000]; 
    int sended;
    while(1)
    {
    gets(message);
    sended = send(description, message, strlen(message), 0);
    if (sended == -1)
    {
        printf("Sending error\n");
        exit(1);
    }
    printf("The message is sended\n");
    }
}
*/



int Accepting_package()
{
    int recieved_from_server;
    while(1)
    {
        //=====================================================================
        bzero(&answering,sizeof(answering));
        //=====================================================================
        recieved_from_server = recv(description,&answering, sizeof(answering), 0);
        if (memcmp(answering.name, PROTOCOL_NAME, 4)!=0)
        {
            printf("Это не мой протокол!\n");
        }
        if (memcmp(answering.version, PROTOCOL_VERSION, 4)!=0)
        {
            printf("Неверная версия протокола!\n");
        }
        switch(answering.type)
        {   
            case PROTOCOL_ANSWER_YES:
                printf("Вход выполнен успешно!\n");
                memcpy(account, sign.login_name,40);
                Command_From_User(account);
                break;
            case PROTOCOL_ANSWER_NO:
                    printf("Неверный логин или пароль\n");
                    Sign_In();
                    break;
            case PROTOCOL_ANSWER_NEW_USER_NO:
                printf("Ошибка! Пользователь с таким ником/почтой уже существует\n");
                New_user();
                break;
            case PROTOCOL_ANSWER_NEW_USER_YES:
                printf("Новый аккаунт успешно создан!\n");
                Sign_In();
                break;
            case PROTOCOL_TAKE_A_PATH:
                printf("Путь до папки с вашим облаком: %s\n", answering.email);
                return 0;
            case PROTOCOL_ERASE_ACCOUNT_YES:
                printf("Ваш аккаунт успешно удален!\n");
                printf("\n\n\n\n\n\n\n\n\n");
                inquiry();
                break;
            case PROTOCOL_ERASE_ACCOUNT_NO:
                printf("При удалении аккаунта произошла ошибка!\n");
                Command_From_User(account);
            default:
                printf("Неизвестная ошибка\n");
                inquiry();
                break;
        }
    } 
}


int Creating_Connection()
{
    // Прописываем ip и порт для соединения с сервером
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons( 5445 );
    //Присоединяемся к серверу. Если неудачно (-1), выводим ошибку 
    if (connect(description,(struct sockaddr *)&server,sizeof(server))==-1)
    {
        printf("Connection error\n");
        exit(1);
    }
    // Иначе идем дальше
    printf("Connected!\n");
    return 0;
}

int main()
{
    Creating_Socket();
    Creating_Connection();
    inquiry();
    Accepting_package();
    return