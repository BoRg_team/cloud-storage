#ifndef Commands
#define Commands
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dirent.h"
#include "erase.h"




void Command_From_User(char *account)
{
    printf("Добро пожаловать, %s! Для помощи введите help\n",account);
    char command[7];
    while(1)
    {
        printf("Введите команду: ");
        command[0]='\0';
        fgets(command, sizeof(command),stdin);
        printf("\n");
        if(strcmp(command,"help")==0)
        {
            printf("Добро пожаловать в помощь!\n");
            printf("Список команд, работающих в данной программе:\n");
            printf("1. help - помощь по программе\n");
            printf("2. send - отправить файл на облако\n");
            printf("3. list - показать список файлов");
            printf("3. delete - удалить файл или папку из облака\n");
            printf("4. create - создать файл или папку\n");
            printf("5. edit - редактировать файл (если возможно)\n");
            printf("6. copy - копировать файл или папку\n");
            printf("7. cut - вырезать файл или папку\n");
            printf("8. rename - переименовать\n");
            printf("9. quit - выйти из аккаунта\n");
            printf("10. about - о программе\n");
            printf("11. erase - полностью удалить аккаунт вместе с данными\n");

        }
        else if(strncmp(command,"send",4)==0)
        {
            Select_file(account);
        }
        else if(strncmp(command,"list",4)==0)
        {
            Take_a_Path(account);
            Accepting_package();
        }
        else if(strncmp(command,"delete",6)==0)
        {
            
        }
        else if(strncmp(command,"create",6)==0)
        {
            
        }
        else if(strncmp(command,"edit",4)==0)
        {
            
        }
        else if(strncmp(command,"copy",4)==0)
        {
            
        }
        else if(strncmp(command,"cut",3)==0)
        {
            
        }
        else if(strncmp(command,"rename",6)==0)
        {
            
        }
        else if(strncmp(command,"quit",4)==0)
        {
            printf("\n\n\n\n");
            inquiry();
            Accepting_package();
        }
        else if(strncmp(command,"about",5)==0)
        {
            
        }
        else if(strncmp(command,"erase",5)==0)
        {
            printf("                    Внимание!\n");
            printf("Вы точно хотите удалить свой аккаунт с сервера?\n");
            printf("Все данные будут утеряны безвозвратно!\n");
            printf("Чтобы заного пользоваться аккаунтом\n");
            printf("Нужно зарегестрировать в системе BoRg\n");
            printf("Введите YES, чтобы удалить аккаунт, или NO, чтобы вернуться\n");
            char answer[3];
            scanf("%s",answer);
            while(1)
            {
                if (strcmp(answer,"YES") == 0) {
                    erase_account(sign.login_name); 
                    Accepting_package();
                    break;
                }
                else if (strcmp(answer, "N") == 0) break;
                else printf("Неизвестная команда!");
            }
        }
        else if(command[0] == '\n') continue;
        else printf("Неизвестная команда!\n");
  }
}
