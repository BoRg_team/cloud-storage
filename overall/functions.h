#ifndef functions
#define functions
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "dirent.h"
#include "../protocol/protocol.h"


int description;

struct _PROTOCOL path;

int Creating_Socket()
{

    // Создаем сокет (соединение) с сервером через интернет в режиме STREAM - 
    // потоковая передача данных.
    description = socket(AF_INET, SOCK_STREAM, 0);
    // Если description равен -1, то сокет не создан
    if (description==-1)
    {
        printf("Error, socket is not created");
        exit(1);
    }
    // иначе сокет установлем, устанавливаем канал связи
    printf("Socket is created\n");
    return 0;
}

void Take_a_Path(char *login)
{
    memcpy(path.name, PROTOCOL_NAME,4);
    memcpy(path.version, PROTOCOL_VERSION,4);
    path.type = PROTOCOL_TAKE_A_PATH;
    memcpy(path.login_name, login,40);
    send(description,&path,sizeof(path),0);
}

/* 1. Как сохранить в массиве названия файлов, лежащих в папке?
   2. Как вернуть этот массив массивов из функции.
   3. Как это вообще работает?!?!*/




/*

DirListNode i;
for (i = head; i != 0; i = i->next) {
    printf("%s\n", i->dir.d_name)
}


1. либо связанный список либо выделение памяти через malloc
2. если через маллок то можно возвращать указатель, либо можно передавать указатель на буфекр куда необходимо поместить элементы
3. 
*/

int List_of_files()
{
    DIR           *d;
  struct dirent *dir;
  d = opendir(".");
  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      printf("%s\n", dir->d_name);
    }

    closedir(d);
  }
  return 0;
}
#endif