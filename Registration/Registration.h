#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "../protocol/protocol.h"


struct _PROTOCOL sign; 


int Sign_In()
{
    char login[40], password[40];
    printf("Введите логин: ");
    scanf("%s",login);
    printf("Введите пароль: ");
    scanf("%s",password);

    memcpy(sign.name, PROTOCOL_NAME,4);
    memcpy(sign.version, PROTOCOL_VERSION,4);
    sign.type = PROTOCOL_SIGN_IN;
    memcpy(sign.login_name, login,40);
    memcpy(sign.password_name,password,40);
    send(description,&sign,sizeof(sign),0);
    printf("Отправлено!\n");
}

int New_user()
{
    char new_login[40],new_password[40],new_email[60];
    printf("Введите ваш логин: ");
    scanf("%s",new_login);
    printf("Введите пароль: ");
    scanf("%s",new_password);
    printf("Введите почту: ");
    scanf("%s",new_email);
    memcpy(sign.name, PROTOCOL_NAME,4);
    memcpy(sign.version, PROTOCOL_VERSION,4);
    sign.type = PROTOCOL_NEW_USER;
    memcpy(sign.login_name, new_login,40);
    memcpy(sign.password_name,new_password,40);
    memcpy(sign.email,new_email,60);
    send(description,&sign,sizeof(sign),0);
    printf("Отправлено!\n");
}


int inquiry()
{
    char answer[2];
    printf("Привет! Добро пожаловать на BoRg облако!\n");
    printf("У вас уже имеестя аккаунт?\n");
    printf("Введите Y, если есть; N, если нет и хотите зарегестрироваться\n");
    while(1)
    {
        scanf("%s",answer);
        if(strcmp(answer,"Y") == 0) {Sign_In(); break;}
        else if (strcmp(answer, "N") == 0) {New_user(); break;}
        else if (strcmp(answer, "q") == 0) 
        {
            printf("До свидания, с вами была компания BoRg! Удачи :)\n");
            exit(3);
        }
        else printf("Ошибка! Проверьте правильность введенной команды\n");
    }
}
