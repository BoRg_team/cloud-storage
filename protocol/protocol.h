#ifndef PROTOCOL_BORG
#define PROTOCOL_BORG

#include <stdint.h>

#define PROTOCOL_NAME "BoRg"
#define PROTOCOL_VERSION "0.01"

#pragma pack(push, 1)

enum CONSTANTS_TYPE
{
    _0_Packet_Type,

    PROTOCOL_SIGN_IN,
    PROTOCOL_NEW_USER,

    PROTOCOL_ANSWER_YES,
    PROTOCOL_ANSWER_NO,

    PROTOCOL_ANSWER_NEW_USER_YES,
    PROTOCOL_ANSWER_NEW_USER_NO,

    PROTOCOL_TAKE_A_PATH,

    PROTOCOL_ERASE_ACCOUNT,
    PROTOCOL_ERASE_ACCOUNT_YES,
    PROTOCOL_ERASE_ACCOUNT_NO,

    PROTOCOL_DATA
};



typedef struct _PROTOCOL
{
    char name[4]; // Имя протокола
    char version[4]; // Версия протокола
    uint16_t type; // Тип данных
    char login_name[40];
    char password_name[40];
    char email[60];
    uint16_t size_data;
    char data[4096];
} SIGNIN;


#pragma pack(pop)

#endif 